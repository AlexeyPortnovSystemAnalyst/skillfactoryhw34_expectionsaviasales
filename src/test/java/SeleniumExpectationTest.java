import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class SeleniumExpectationTest {

    private static final String URL = "https://www.aviasales.ru/search/KRR2909MOW27101?request_source=search_form&payment_method=all";
    private static final String SUBSCRIPTION_BUTTON_CLASS = "listing-header";
    private static final String PREDICTION_FIRST_COLUMN_CLASSNAME = "content_342b1a5"; //content_342b1a5 _6agN54LH9Ui5nV-ZxqvhX hideOverflowContent_342b1a5

    private static WebDriver driver;

    @BeforeAll
    public static void setUpWebDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\89672\\OneDrive\\Документы\\projects\\AlexMhal\\chromedriver.exe");
        driver = new ChromeDriver();
        //driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5)); //неявные ожидания

    }

    @Test
    public void subscription_button_appears() {
        driver.get(URL);
        Assertions.assertDoesNotThrow(()->driver.findElement(By.className(SUBSCRIPTION_BUTTON_CLASS)));
        }

    @Test
    public void prediction_button_appears() {
        driver.get(URL);
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        Assertions.assertDoesNotThrow(()-> driverWait.until(ExpectedConditions.presenceOfElementLocated(By.className(PREDICTION_FIRST_COLUMN_CLASSNAME))));
    }

}